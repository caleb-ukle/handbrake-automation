# Handbrake Automation
Run handbrake recursively on an _ingest_ folder and recreate in your media folder.

## How to use
requires 2 input arguments
1. path\to\handbrake-cli
1. path\to\media\folder

There are some assumptions taken with folder structure
1. The ingest folder is located in your media folder under `/ingest`
1. Converted media in `/ingest`  will be moved to up one level and will match folder structure in the `/ingest` folder

### Example
The media folder is located in `/media` or `M:\media`, therefore the ingest (or place where media to be transcode) folder is located at `/media/ingest` or `M:\media\ingest`

Running the program at the command line would look like
```bash
handbrake-automation path/to/handbrake-cli /media/
```
```powershell
handbrake-automation.exe path\to\handbrake-cli M:\media\
```

> Note the end slash on the media path (second arg)

This tool transcodes each file one at a time just like the handbrake GUI. 

## Presets
I included two presets one for regular, every day media, the other for hand animated (such as Studio Ghibli). Everything will be encoded using the `film.json` preset unless the next `(animation)` is present in the file name. When `(animation)` is present in the file name it will use the `animation.json` present and also **remove the text `(animaiton)` from the file name for the transcoded output.