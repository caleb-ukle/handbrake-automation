﻿using System;
using System.Collections.Generic;
using System.Linq;
using handbrake_automation.util;

namespace handbrake_automation
{
    class Program
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="args">
        /// 0: handbrake cli path
        /// 1: media path
        /// </param>
        static void Main(string[] args)
        {
            foreach (var a in args)
            {
                Console.WriteLine(a);
            }

            if (args.Length < 2)
                throw new ArgumentException(
                    $"Expected 2 arguments, got {args.Length}. Expected Handbrake cli path, media path in that order.");

            var cliPath = args[0];
            var mediaPath = args[1];
            var ingestPath = $"{mediaPath}ingest";

            var itemsToConvert = GetMedia(ingestPath, mediaPath);
            foreach (var item in itemsToConvert)
            {
                var didConvert = item.Convert(cliPath);
                if (didConvert)
                {
                    Console.WriteLine($"Successfully converted {item.FileName}");
                    Helpers.DeleteFile(item.InputPath);
                    Console.WriteLine($"Deleting source file");
                }
                else
                {
                    Console.WriteLine($"Issue converting {item.FileName} at {item.InputPath}");
                    Console.WriteLine($"Leaving source file in place");
                }
            }
        }


        private static IEnumerable<ConversionMetaItem> GetMedia(string ingestPath, string mediaFolder)
        {
            var files = Helpers.GetDirectoryContents(ingestPath);

            return files.Select(f => new ConversionMetaItem(f, mediaFolder));
        }
    }
}