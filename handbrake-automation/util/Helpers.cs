using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace handbrake_automation.util
{
    public static class Helpers
    {
        public static IEnumerable<string> GetDirectoryContents(string path)
        {
            var files = new List<string>();

            foreach (var f in Directory.GetFiles(path))
            {
                files.Add(f);
            }

            foreach (var d in Directory.GetDirectories(path))
            {
                files.AddRange(GetDirectoryContents(d));
            }

            return files.Where(f => f.EndsWith(".mov", StringComparison.OrdinalIgnoreCase)
                                    || f.EndsWith(".mkv", StringComparison.OrdinalIgnoreCase)
                                    || f.EndsWith(".mp4", StringComparison.OrdinalIgnoreCase));
        }

        public static ConversionType ParseFileType(string path)
        {
            return path.Contains("(animation)", StringComparison.OrdinalIgnoreCase)
                ? ConversionType.Animation
                : ConversionType.Film;
        }

        public static string ParseOutputPath(string filePath, string mediaPath)
        {
            var ext = Path.GetExtension(filePath);
            var parts = filePath.Split($"{mediaPath}ingest/");
            if (parts.Length < 2)
                throw new ArgumentException(
                    $"Media path is not apart of the File Path. Expected to fine {mediaPath} in {filePath}");

            var formattedPath = parts[1].Replace(ext, ".mkv").Replace("(animation)", "");

            var output = $"{mediaPath}{formattedPath}";
            var parentDir = Path.GetDirectoryName(output);
            if (!Directory.Exists(parentDir))
            {
                Directory.CreateDirectory(parentDir);
            }

            return output;
        }

        public static void DeleteFile(string path)
        {
            File.Delete(path);
        }
    }

    public enum ConversionType
    {
        Film,
        Animation
    }
}