using System;
using System.Diagnostics;
using System.IO;
using handbrake_automation.util;

namespace handbrake_automation
{
    public class ConversionMetaItem
    {
        public string FileName { get; }

        public string InputPath { get; }

        private string OutputPath { get; }

        private ConversionType Type { get; }

        public ConversionMetaItem(string filePath, string mediaPath)
        {
            FileName = Path.GetFileName(filePath);
            InputPath = filePath;
            OutputPath = Helpers.ParseOutputPath(filePath, mediaPath);
            Type = Helpers.ParseFileType(filePath);
        }

        public bool Convert(string cliPath)
        {
            var startInfo = new ProcessStartInfo
            {
                CreateNoWindow = false,
                UseShellExecute = false,
                FileName = cliPath,
                WindowStyle = ProcessWindowStyle.Hidden,
                Arguments = BuildArgs()
            };

            using var process = Process.Start(startInfo);
            process?.WaitForExit();
            return process?.ExitCode == 0;
        }

        private string BuildArgs()
        {
            string presetPath;
            string presetName;

            switch (Type)
            {
                case ConversionType.Film:
                    presetName = "Film tune 1080p BluRay";
                    presetPath = Path.Combine(Directory.GetCurrentDirectory(), "./presets/film.json");
                    break;
                case ConversionType.Animation:
                    presetName = "animation";
                    presetPath =
                        Path.Combine(Directory.GetCurrentDirectory(), "./presets/animation.json");
                    break;
                default:
                    throw new ArgumentOutOfRangeException($"Unspecified Type, got {Type}");
            }

            if (!File.Exists(presetPath))
                throw new ApplicationException($"Unable to find preset file. Invalid file path {presetPath}");

            return $"-i \"{InputPath}\" -o \"{OutputPath}\" --preset-import-file \"{presetPath}\" -Z \"{presetName}\"";
        }
    }
}