#! /bin/sh

dotnet publish -c release -r linux-x64
dotnet publish -c release -r osx-x64
dotnet publish -c release -r win-x64
